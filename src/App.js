import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { URL } from './config/config';
import axios from 'axios';
import Users from './components/Users';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import EditUser from './components/EditUser';
import AddUser from './components/AddUser';

axios.defaults.baseURL = URL();

class App extends Component {
	render() {
		return (
			<Router>
				<div className="App">
					<div className="container">
						<Switch>
							<Route exact path="/" component={Users} />
							<Route exact path="/edit/:id" component={EditUser} />
							<Route exact path="/add" component={AddUser} />
						</Switch>
					</div>
				</div>
			</Router>
		);
	}
}

export default App;
