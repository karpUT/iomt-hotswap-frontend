import React from 'react';
import { LineChart, Line, XAxis, YAxis, ResponsiveContainer, Tooltip, Legend } from 'recharts';
import axios from 'axios';
const formatData = data => {
	const result = [];

	data.docs.forEach(record => {
		record.heartRates.forEach(rate => result.push({ heartrate: rate }));
	});

	return result;
};

class HrChart extends React.Component {
	state = {
		data: [],
	};

	componentDidMount() {
		axios
			.get('/firebase/hrData', { params: { user: this.props.user } })
			.then(res => {
				this.setState({
					data: formatData(res.data),
				});
			})
			.catch(err => {
				console.log(err);
			});
	}

	render() {
		return (
			<ResponsiveContainer>
				<LineChart data={this.state.data} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
					<Line type="monotone" dataKey="heartrate" stroke="#8884d8" dot={false} />
					<XAxis dataKey="time" />
					<YAxis />
					<Tooltip />
					<Legend />
				</LineChart>
			</ResponsiveContainer>
		);
	}
}

export default HrChart;
