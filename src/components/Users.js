import React, { Component } from 'react';
import axios from 'axios';
import UserCard from './UserCard';

export default class Users extends Component {
	state = {
		users: [],
	};

	componentDidMount() {
		axios
			.get('/firebase/users')
			.then(response => {
				this.setState({
					users: response.data.docs,
				});
			})
			.catch(err => {
				console.log('error');
				this.setState({
					users: [],
				});
			});
	}

	render() {
		return (
			<div>
				<div className="card blue darken-1">
					<div className="card-content white-text">
						<span className="card-title">
							Add User
							<button
								className=" btn-floating btn waves-effect waves-light red right z-depth-3"
								onClick={() => {
									this.props.history.push('/add');
								}}
							>
								+
							</button>
						</span>
					</div>
				</div>

				{this.state.users.map(user => (
					<UserCard user={user} key={user.id} history={this.props.history} />
				))}
			</div>
		);
	}
}
