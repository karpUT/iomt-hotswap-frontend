import React, { PureComponent } from 'react';
import HrCollection from './HrCollection';
import axios from 'axios';

export default class EditUser extends PureComponent {
	state = {
		id: this.props.match.params.id,
		First: '',
		Last: '',
		token: '',
		filepath: '',
		PolarID: '',
		doctorPhone: '',
	};

	componentDidMount() {
		let id = this.state.id;
		axios
			.get('/firebase/user', { params: { id: id } })
			.then(res => {
				this.setState({
					...res.data,
					id: id,
				});
			})
			.catch(err => {
				console.log(err);
			});
	}

	fileSelectedHandler = event => {
		this.setState({ selectedFile: event.target.files[0] });
	};

	fileUploadHandler = async () => {
		const file = this.state.selectedFile;
		if (file === undefined || file === null) return;

		const fd = new FormData();
		fd.append('File', file, file.name);

		try {
			let res = await axios.post('/firebase/uploadFile', fd, {
				headers: { 'content-type': 'multipart/form-data' },
			});
			this.setState({
				...this.state,
				filepath: res.data.url,
			});
		} catch (err) {
			console.log(err);
		}
	};

	onSubmit = e => {
		const { id, First, Last, PolarID, filepath, doctorPhone } = this.state;
		const fields = {
			First: First,
			Last: Last,
			filepath: filepath,
			PolarID: PolarID,
			doctorPhone: doctorPhone,
		};

		e.preventDefault();
		if (this.state.id) {
			axios
				.post('/firebase/update', { id: id, fields: fields })
				.then(res => {
					this.props.history.push('/');
				})
				.catch(err => {
					console.log(err);
				});
		} else {
			alert('No user id found');
		}
	};

	render() {
		return (
			<div className="row">
				<div className="col s12 card-panel teal darken-1 center white-text">
					<h5>Edit User</h5>
				</div>

				<HrCollection user={this.state.id} token={this.state.token} hrCritical={this.state.hrCritical} />

				<form className="col s12" onSubmit={this.onSubmit}>
					<div className="row">
						<div className="input-field col s12">
							<input type="text" id="id" value={this.state.id} readOnly={true} />
							<span className="helper-text" data-error="wrong" data-success="right">
								Firestore Id
							</span>
						</div>

						<div className="input-field col s6">
							<input
								type="text"
								id="first"
								value={this.state.First}
								onChange={e => {
									this.setState({
										...this.state,
										First: e.target.value,
									});
								}}
							/>
							<span className="helper-text" data-error="wrong" data-success="right">
								First Name
							</span>
						</div>

						<div className="input-field col s6">
							<input
								type="text"
								id="last"
								value={this.state.Last}
								onChange={e => {
									this.setState({
										...this.state,
										Last: e.target.value,
									});
								}}
							/>
							<span className="helper-text" data-error="wrong" data-success="right">
								Last Name
							</span>
						</div>

						<div className="input-field col s12">
							<input
								type="text"
								id="PolarID"
								value={this.state.PolarID}
								onChange={e => {
									this.setState({
										...this.state,
										PolarID: e.target.value,
									});
								}}
							/>
							<span className="helper-text" data-error="wrong" data-success="right">
								Polar ID
							</span>
						</div>
						<div className="input-field col s12">
							<input
								type="text"
								id="doctorPhone"
								value={this.state.doctorPhone}
								onChange={e => {
									this.setState({
										...this.state,
										doctorPhone: e.target.value,
									});
								}}
							/>
							<span className="helper-text" data-error="wrong" data-success="right">
								Supervisor's Phone
							</span>
						</div>

						<div className="input-field col s12">
							<input type="text" id="file" value={this.state.filepath} readOnly={true} />
							<span className="helper-text" data-error="wrong" data-success="right">
								Dex File
							</span>
						</div>
					</div>

					<div className="input-field col s8">
						<input id="uploadField" type="file" accept=".dex" onChange={this.fileSelectedHandler} />
					</div>
					<div className="col s4">
						<button
							type="button"
							id="uploadButton"
							className="btn-small "
							onClick={this.fileUploadHandler}
							disabled={this.state.selectedFile ? false : true}
						>
							Upload Selected File
						</button>
					</div>
					<div className="col s12">
						<input type="submit" value="Save Changes" className="btn " />
						<button
							type="button"
							className="red btn"
							onClick={() => {
								this.props.history.goBack();
							}}
						>
							<span>Cancel</span>
						</button>
					</div>
				</form>
			</div>
		);
	}
}
