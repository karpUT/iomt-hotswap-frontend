import React from 'react';
import HrChart from './HrChart';

function HrCollection({ token, user, hrCritical }) {
	return (
		<div className="collection">
			<div className={'collection-item ' + (hrCritical ? ' red ' : ' green ') + ' center white-text'}>
				<h6>HEARTRATE STATUS</h6>
			</div>
			<div style={{ width: '100%', height: 300 }} className="collection-item center">
				<HrChart user={user} />
			</div>
			<div className="collection-item center">
				<small>
					<b>TOKEN</b>
					<br />
					<span>{token}</span>
				</small>
			</div>
		</div>
	);
}

export default HrCollection;
