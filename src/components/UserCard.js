import React, { Component } from 'react';
import axios from 'axios';

export default class UserCard extends Component {
	editContact = () => {
		this.props.history.push('/edit/' + this.props.user.id);
	};

	pushFile = () => {
		axios
			.post('/firebase/pushFile', { user: this.props.user.id })
			.then(res => {
				alert('File pushed to device');
			})
			.catch(err => {
				alert('Failed to push file to device');
			});
	};

	render() {
		return (
			<div className={'card ' + (this.props.user.hrCritical ? 'red' : 'green')}>
				<div className="card-content white-text">
					<span className="card-title">
						{this.props.user.First} {this.props.user.Last}
					</span>
					<table>
						<tbody>
							<tr>
								<td className="key">
									<b>Id:</b>
								</td>
								<td className="value">{this.props.user.id}</td>
							</tr>
							<tr>
								<td>
									<b>Polar ID:</b>
								</td>
								<td>{this.props.user.PolarID}</td>
							</tr>
							<tr>
								<td>
									<b>File:</b>
								</td>
								<td>{this.props.user.filepath}</td>
							</tr>
							<tr>
								<td>
									<button
										className="waves-effect waves-light btn z-depth-2"
										onClick={this.editContact}
									>
										View
									</button>
								</td>
								<td>
									<button className="waves-effect waves-light btn z-depth-2" onClick={this.pushFile}>
										Push File To Device
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}
