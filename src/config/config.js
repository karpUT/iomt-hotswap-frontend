function getBaseURL() {
	if (process.env.REACT_APP_MODE === 'DEV') {
		return 'http://localhost:5000';
	}
	return 'http://ec2-13-53-188-126.eu-north-1.compute.amazonaws.com';
}

module.exports = {
	URL: getBaseURL,
};
