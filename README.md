### Front End Application for Bachelor's thesis:
**Android-based Fog Computing Gateway with Over-The-Air Programming for personalized Health Monitoring**

by Kristo Karp

---

> This service can be found running at http://13.53.188.126:3000/#/ until 20th of June 2019.

---

## Introduction

This is the front end part for the thesis's web service. The guide to using the service can be found in the thesis in section 4.1

---

## System Requirements

-   node version 10+
-   npm version 6+

## Available Scripts

In the project directory, you can run the following commands to use the service:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
This uses the previously set up back end service URL as a base URL for all requests.

_URL:_ `http://ec2-13-53-188-126.eu-north-1.compute.amazonaws.com`

### `npm run start:dev`

Will do exactly the same thing as `npm start`, but URL for the the API server is set to `http://localhost:5000` so that you can use the API server that you have set up locally.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

---
> In case of any further questions please contact Kristo.Karp[at]gmail.com
